import * as L from 'leaflet'
import 'leaflet.fullscreen'
import 'leaflet.fullscreen/Control.FullScreen.css'
import 'leaflet/dist/leaflet.css'
import FhcLeaflet from './src/FhcLeaflet.vue'
import { xy } from './src/LeafletUtil'
export { FhcLeaflet }
export { L }

export { xy } from './src/FhcLeaflet'
