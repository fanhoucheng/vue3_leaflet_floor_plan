import HeatMap from '../../Heatmap/index'
import * as L from 'leaflet'
import { type StoreData } from '../../Heatmap/index'

export class HeatLeafletMap {
  private divImgLayer: HTMLElement //地图里楼层图外层的div
  private imgLayerEL: HTMLImageElement //地图里楼层图的图片元素

  private heatmap: HeatMap | null = null
  private heatmapCanvas: HTMLCanvasElement
  private map: L.Map
  private isDestory = true
  private imageOverlay: L.ImageOverlay
  private divImgResizeObserver: ResizeObserver | null = null
  private zoomstartFn?: L.LeafletEventHandlerFn | null = null

  constructor(map: L.Map, imageOverlay: L.ImageOverlay) {
    this.map = map
    this.imageOverlay = imageOverlay
    //这里先把图元素找出，方便后面画heatmap等
    this.imgLayerEL = imageOverlay.getElement()!
    // = document.querySelector(
    //   'img.leaflet-image-layer.leaflet-zoom-animated',
    // ) as HTMLElement;
    console.log('HeatLeafletMap', this.imgLayerEL.clientWidth, this.imgLayerEL.clientHeight)
    this.divImgLayer = this.imgLayerEL.parentElement!
    // document.querySelector(
    //   'div.leaflet-pane.leaflet-overlay-pane',
    // ) as HTMLElement;

    const width = Number(this.imgLayerEL.style.width.replace('px', ''))
    const height = Number(this.imgLayerEL.style.height.replace('px', ''))
    let r = this.imgLayerEL.clientWidth / 80
    r = r + 4
    this.heatmap = new HeatMap({
      className: 'heatmap-canvas',
      container: this.divImgLayer,
      maxOpacity: 0.6,
      radius: r,
      blur: 0.9,
      width: width,
      height: height
    })
    const that = this.heatmap
    this.heatmapCanvas = document.getElementsByClassName('heatmap-canvas')[0] as HTMLCanvasElement
    this.heatmapCanvas.style.transform = this.imgLayerEL.style.transform
    this.heatmapCanvas.style.width = this.imgLayerEL.style.width
    this.heatmapCanvas.style.height = this.imgLayerEL.style.height
    const self = this

    this.isDestory = false
    this.zoomstartFn = function (_e) {
      console.log('zoomstart3333', self.imageOverlay.getBounds())
      if (!self.isDestory) {
        if (self.heatmap) {
          self.hidden()
        }
      }
    }
    map.on('zoomstart', this.zoomstartFn)

    //ResizeObserver 代替
    // map.on('zoomend', function (_e) {
    //   if (!self.isDestory) {
    //     console.log(map.getZoom(), that.config);
    //     if (self.heatmap) {
    //       self.show();
    //     }
    //   }
    // });

    this.divImgResizeObserver = new ResizeObserver(() => {
      if (!self.isDestory) {
        if (self.heatmap) {
          self.show()
        }
      }
    })
    this.divImgResizeObserver.observe(this.imgLayerEL)
  }

  getCanvasSize(): number[] {
    const width = Number(this.heatmapCanvas.style.width.replace('px', ''))
    const height = Number(this.heatmapCanvas.style.height.replace('px', ''))
    return [width, height]
  }
  hidden() {
    this.heatmapCanvas.style.display = 'none'
  }
  destory() {
    if (this.heatmapCanvas && this.heatmapCanvas.parentNode) {
      this.heatmapCanvas.parentNode.removeChild(this.heatmapCanvas)
    }
    this.heatmap = null
    this.isDestory = true
    this.divImgResizeObserver?.disconnect()
    if (this.zoomstartFn) this.map.off('zoomstart', this.zoomstartFn)
    // this.map.off('zoomend');
  }

  resetImageOverlay(imageOverlay: L.ImageOverlay) {
    this.imageOverlay = imageOverlay
    //这里先把图元素找出，方便后面画heatmap等
    this.imgLayerEL = imageOverlay.getElement()!
    // = document.querySelector(
    //   'img.leaflet-image-layer.leaflet-zoom-animated',
    // ) as HTMLElement;
    console.log('HeatLeafletMap', this.imgLayerEL.clientWidth, this.imgLayerEL.clientHeight)
    this.divImgLayer = this.imgLayerEL.parentElement!
  }
  show() {
    if (this.heatmapCanvas) {
      this.heatmapCanvas.style.display = 'block'
      this.heatmapCanvas.style.transform = this.imgLayerEL.style.transform
      this.heatmapCanvas.style.width = this.imgLayerEL.style.width
      this.heatmapCanvas.style.height = this.imgLayerEL.style.height

      // this.heatmapCanvas.animate(
      //   // 关键帧数量大于1的时候必须要用数组包裹 , 只有一个关键帧时也可以使用数组包裹
      //   {
      //     width: this.imgLayerEL.style.width,
      //     height: this.imgLayerEL.style.height,
      //     display : 'block',
      //     transform : this.imgLayerEL.style.transform
      //   },
      //   {
      //     duration: 2, // 持续时间
      //     easing: 'linear', // 规定动画的速度曲线
      //     delay: 0,// 在动画开始之前的延迟
      //     iterations: 1,// 动画应该播放的次数
      //     direction: "alternate", // 是否应该轮流反向播放动画
      //     fill: 'forwards',// 动画是否停在最后一个关键帧
      //   }
      // )
    }
  }
  setData(data: StoreData) {
    let ratio = 1
    let ele = this.imageOverlay.getElement()
    if (ele) {
      ratio = ele.getBoundingClientRect().width / this.map.imageMapWidth
    }

    let tempData: StoreData = { max: data.max, data: [] }
    data.data.forEach((item) => {
      tempData.data.push({
        x: Math.ceil(item.x * ratio),
        y: Math.ceil(item.y * ratio),
        value: item.value
      })
    })
    this.heatmap?.setData(tempData)
  }
  getData(): StoreData | undefined {
    return this.heatmap?.getData()
  }
}
