import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/socket',
      name: 'socket',
      component: () => import('../views/SocketView.vue'),
      meta: {
        keepAlive: true,
        title: 'Socket'
      }
    },
    {
      path: '/microService',
      name: 'microService',
      component: () => import('../views/MicroServiceView.vue'),
      meta: {
        keepAlive: true,
        title: 'Socket'
      }
    },
    {
      path: '/about',
      name: 'about',
      // components: {
      //   keepAlive: () => import('../views/AboutView.vue')
      // },
      component: () => import('../views/AboutView.vue'),
      meta: {
        keepAlive: true,
        title: 'About'
      }
    }
  ]
})

export default router
