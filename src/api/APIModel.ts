export interface DtoBase {
  code: number
  message: string
  data: {} | null
}
export interface DtoPageBase {
  code: number
  message: string
  data: {
    pageSize: string
    totalCount: number
    data: DtoBase[] | null
  } | null
}

export interface DtoUser extends DtoBase {}
export interface DtoOrder extends DtoBase {}
export interface DtoOrderDetail extends DtoBase {}
export interface DtoProduct extends DtoBase {}

export interface DtoUserProfile extends DtoBase {
  data: {
    profile: {
      orders: any[]
      sses: any[]
    }
  } | null
}

export interface DtoUpload extends DtoBase {
  uploadKey: string
  uploadType: string
}
