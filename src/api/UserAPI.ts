import service from './Request' //引入api
import type {
  DtoBase,
  DtoPageBase,
  DtoUser,
  DtoOrder,
  DtoProduct,
  DtoUserProfile
} from './APIModel' //引入api
import { DownloadType } from './CommonAPI'
//引入api

export async function userLogin(username: string, password: string): Promise<DtoUser> {
  try {
    const result = (await service.post('/user/login', {
      username: username,
      password: password
    })) as DtoUser
    return result
  } catch (error) {
    console.log('userLogin API >>>>', error)
    return { code: -1, message: error + '' } as DtoUser
  }
}

export async function userRegist(username: string, password: string): Promise<DtoUser> {
  try {
    const result = (await service.post('/user/regist', {
      username: username,
      password: password
    })) as DtoUser
    return result
  } catch (error) {
    console.log('userRegist API >>>>', error)
    return { code: -1, message: error + '' } as DtoUser
  }
}

export async function userProfile(): Promise<DtoUserProfile> {
  try {
    const result = (await service.get('user/profile', {})) as DtoUserProfile
    return result
  } catch (error) {
    console.log('userProfile API >>>>', error)
    return { code: -1, message: error + '' } as DtoUserProfile
  }
}
export async function getFile(downloadType: DownloadType, downloadKey?: DownloadType) {
  try {
    let currentTime_stamp = new Date().getTime()
    const res = await service.post(
      '/file/download/' + downloadType + '?timestamp=' + currentTime_stamp,
      { downloadType: downloadType, downloadKey: downloadKey },
      { responseType: 'blob' }
    )
    if (res.data instanceof Blob) {
      const href = window.URL.createObjectURL(res.data)
      return href // 获取到的图片路径
    }
  } catch (error) {
    console.log('getFile API >>>>', error)
  }
}

export async function home(): Promise<DtoBase> {
  try {
    const result = (await service.post('home', {})) as DtoBase
    return result
  } catch (error) {
    console.log('home API >>>>', error)
    return { code: -1, message: error + '' } as DtoBase
  }
}

export async function createExcel() {
  try {
    const res = await service.post('/user/excel', {}, { responseType: 'blob' })
    const blob = res.data
    const objectUrl = URL.createObjectURL(blob) //创建下载的链接
    const a = document.createElement('a')
    a.href = objectUrl
    let fileName = 'test.pdf'
    fileName = res.headers['content-disposition'].split('filename=')[1]
    fileName = decodeURI(fileName)
    console.log('download filename', fileName, res.headers['content-length'])
    a.download = fileName //设置文件名
    //下面这个写法兼容火狐
    a.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }))
  } catch (error) {
    console.log('createExcel API >>>>', error)
  }
}
export async function addSse(dtos: { name: string; comment?: string }[]): Promise<DtoBase> {
  try {
    const result = (await service.post('/sys/sse/add', dtos)) as DtoBase
    return result
  } catch (error) {
    console.log('sseAdd API error>>>>', error)
    return { code: -1, message: error + '' } as DtoBase
  }
}

export async function addUserSse(sseIds: (string | number)[]): Promise<DtoBase> {
  try {
    const result = (await service.post('/user/sse/add', sseIds)) as DtoBase
    return result
  } catch (error) {
    console.log('sseAdd API error>>>>', error)
    return { code: -1, message: error + '' } as DtoBase
  }
}

export async function addProduct(dto: any): Promise<DtoProduct> {
  try {
    const result = (await service.post('/product/add', dto)) as DtoProduct
    return result
  } catch (error) {
    console.log('productAdd API error>>>>', error)
    return { code: -1, message: error + '' } as DtoProduct
  }
}

export async function addOrder(): Promise<DtoOrder> {
  try {
    const result = (await service.post('/order/add')) as DtoOrder
    return result
  } catch (error) {
    console.log('orderAdd API error>>>>', error)
    return { code: -1, message: error + '' } as DtoOrder
  }
}

export async function getSseList(): Promise<DtoBase> {
  try {
    const result = (await service.post('sys/sse/list')) as DtoBase
    console.log('getSseList result', result)
    return result
  } catch (error) {
    console.log('getSseList API >>>>', error)
    return { code: -1, message: error + '' } as DtoBase
  }
}
export async function getProduct(): Promise<DtoBase> {
  try {
    const result = (await service.get('product/list')) as DtoBase
    console.log('getProduct result', result)
    return result
  } catch (error) {
    console.log('getProduct API >>>>', error)
    return { code: -1, message: error + '' } as DtoBase
  }
}
export async function getPaginateProducts(
  pageNumber: number,
  pageSize?: number
): Promise<DtoPageBase> {
  try {
    let params = {}
    if (pageSize) {
      params = {
        pageSize: pageSize
      }
    }
    const result = (await service.get('product/page/' + pageNumber, {
      params: params
    })) as DtoPageBase
    console.log('getPaginateProducts result', result)
    return result
  } catch (error) {
    console.log('getProduct API >>>>', error)
    return { code: -1, message: error + '' } as DtoPageBase
  }
}

export async function getOrder(userId: number): Promise<DtoBase> {
  try {
    const result = (await service.get('order/user/' + userId)) as DtoBase
    return result
  } catch (error) {
    console.log('getOrder API >>>>', error)
    return { code: -1, message: error + '' } as DtoBase
  }
}

export async function getNews(): Promise<DtoBase> {
  try {
    const result = (await service.post('getNews', {})) as DtoBase
    return result
  } catch (error) {
    console.log('getNews API >>>>', error)
    return { code: -1, message: error + '' } as DtoBase
  }
}

export async function sseSend() {
  try {
    await service.get('sse/send')
  } catch (error) {
    console.log('sseSend API >>>>', error)
  }
}
