import './assets/main.css'
import Antd from 'ant-design-vue'
import 'ant-design-vue/dist/reset.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

import service from './api/Request' //引入request.js

const app = createApp(App)

app.use(createPinia())
app.use(router)

app.provide('$axios', service)
app.use(Antd).mount('#app')
// 全局挂载 axios
app.config.globalProperties.$axios = service //配置axios的全局引用
